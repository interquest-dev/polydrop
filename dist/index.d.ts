export declare function addTranslations(translations: {
    [key: string]: string;
}): void;
export default function t(context: string, label: string, args?: any): string;
