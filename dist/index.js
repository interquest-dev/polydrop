"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Polyglot = require("node-polyglot");
var polyglot = new Polyglot();
function addTranslations(translations) {
    polyglot.extend(translations);
}
exports.addTranslations = addTranslations;
function t(context, label, args) {
    if (args === void 0) { args = null; }
    return polyglot.t(context + "_" + label, args);
}
exports.default = t;
