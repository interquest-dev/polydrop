import * as Polyglot from "node-polyglot";
const polyglot = new Polyglot();

export function addTranslations(
  translations: {[key: string]: string},
) {
  polyglot.extend(translations);
}

export default function t(context: string, label: string, args: any = null) {
  return polyglot.t(`${context}_${label}`, args);
}
