import t, { addTranslations } from "../src/index";

test("Translation api", () => {
  const strings = {
    context_test: "test",
    context_test2: "test 2",
  };
  addTranslations(strings);

  expect(t("context", "test")).toBe("test");
  expect(t("context", "test2")).toBe("test 2");
});
